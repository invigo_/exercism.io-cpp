#pragma once

#include <cmath>

namespace difference_of_squares {

long int square_of_sum(const int& _n);
long int sum_of_squares(const int& _n);
long int difference(const int& _n);

}
