#pragma once

#include <stdexcept>

namespace collatz_conjecture {

unsigned int steps(const int&);

}