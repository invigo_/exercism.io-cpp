#pragma once

#include <cmath>
#include <string>

namespace armstrong_numbers {

    bool is_armstrong_number(const int&);

}  // namespace armstrong_numbers