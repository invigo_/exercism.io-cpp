#pragma once

#include <string>
#include <algorithm>

namespace acronym {

const std::string acronym(const std::string&);

}  // namespace acronym